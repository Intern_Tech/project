import React, { Component } from 'react';
import './App.css';
import NavBar from './components/navbar';
import Counters from "./components/counters";
import MyForm from "./components/nameform";
import EssayForm from "./components/essasyform";
import CreateAccount from "./components/createaccount";


class App extends Component {
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 3 },
      { id: 3, value: 2 },
      { id: 4, value: 1 },
    ],
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  handleDelete = (counterId) => {
    console.log(counterId);
    const counters = this.state.counters.filter((c) => c.id !== counterId);
    this.setState({ counters });
  };

  handleSubmit = (value) => {
    console.log(value);
    alert("A value was submitted: " + value);
  };
  render() {
    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter((c) => c.value > 0).length}
        />
        <main className="container">
          <Counters
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDelete={this.handleDelete}
            counters={this.state.counters}
            onSubmit={this.handleSubmit}
          />
        </main>
        <MyForm />
        <EssayForm />
        <CreateAccount />
      </React.Fragment>
    );
  }
}
 
export default App;
