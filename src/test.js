import React, { Component } from "react";

class Form extends Component {
  constructor() {
    super();
    this.state = { value: " " };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  handleSubmitting(event) {
    alert("A name was submitted: " + this.state.value);
    event.perventDefault();
  }
  render() {
    return (
      <form onSubmit={this.handleSubmitting}>
        <label>
          Name:
          <input
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

export default Form;
